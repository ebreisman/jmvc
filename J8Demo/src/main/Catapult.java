package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandlerContainer;

import router.Router;
import router.RouterHandler;

public class Catapult extends AbstractHandlerContainer {

	private List<Handler> handlers = new ArrayList<Handler>();
	
	public Catapult(Router router) {
		handlers.add(new RouterHandler(router));
	}
	
	@Override
	public Handler[] getHandlers() {
		return (Handler[])handlers.toArray();
	}

	public void addHandler(Handler handler) {
		handlers.add(handler);
	}
	
	@Override
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		for (Handler handler : handlers) {
			handle(target, baseRequest, request, response);
		}

	}

}
