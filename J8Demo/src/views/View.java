package views;

public class View {

	private String result;
	
	public View() { }
	
	public View(String result) {
		this.result = result;
	}
	
	public String getResult() {
		return result;
	}
	
}
