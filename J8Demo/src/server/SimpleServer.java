package server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import router.Router;
import router.RouterHandler;

public class SimpleServer {

	public static void main(String[] args) throws Exception {
//		Initialize Router
		Router router = new Router("controllers");

		for (String route : router.getRoutes()) {
			System.out.println(route.toString());
		}
		
//		Initialize Filters
		
//		AbstractHandler routerHandler = new RouterHandler(router);
		
		Server server = new Server(8081);
		server.setHandler(new RouterHandler(router));

		server.start();
		server.join();
	}

}
