package test.controllers;

import views.View;
import annotations.Controller;
import annotations.GET;
import annotations.POST;

@Controller("test")
public class TestController {
	
	@GET
	public View index() {
		return new View("index");
	}
	
	@GET("different/path")
	public View diffPath() {
		return new View("diffpath");
	}
	
	@GET("non/empty/:param/map")
	public View nonEmptyParam(String param) {
		return new View("nonEmptyParam");
	}
	
	@GET("missingMethodParams")
	public View missingMethodParams() {
		return new View("missingMethodParams");
	}
	
	@POST
	public View postIt() {
		return new View("postIt");
	}
	
	@POST("to/post")
	public View postName() {
		return new View("postName");
	}
	
	@POST("post/:myName")
	public View postWithName(String myName) {
		return new View("postWithName");
	}
}
