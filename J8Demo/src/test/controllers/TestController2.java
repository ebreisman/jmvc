package test.controllers;

import views.View;
import annotations.Controller;
import annotations.GET;

@Controller
public class TestController2 {
	
	@GET
	public View index2() {
		return new View("index2");
	}
	
	@GET("test2ControllerMethod")
	public View index3() {
		return new View("test2ControllerMethod");
	}
}
