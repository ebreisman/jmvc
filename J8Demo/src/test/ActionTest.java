package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import router.Action;
import test.controllers.TestController;
import views.View;

public class ActionTest {

	Action route = null;
	View view = null;
	Class<?> clazz = null;
	Constructor<?> ctor = null;
	Object object = null;
	Map<String, String[]> emptyMap;
	Map<String, String[]> nonEmptyMap;

	@Before
	public void setUp() throws Exception {
		clazz = TestController.class;

		try {
			ctor = clazz.getConstructor(new Class<?>[] { });
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}

		try {
			object = ctor.newInstance(new Object[] { });
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		emptyMap = new HashMap<String, String[]>();
		nonEmptyMap = new HashMap<String, String[]>();
	}

	@Test
	public void testExecuteRouteMethod() throws Exception {
		route = new Action("index", object, TestController.class.getDeclaredMethod("index", new Class<?>[] { }));
		assertNotNull(route);
		
		view = route.execute(emptyMap);
		assertEquals(view.getResult(), "index");
	}
	
	@Test
	public void testExecutePathDiffThanMethodName() throws Exception {
		route = new Action("different/path", object, TestController.class.getDeclaredMethod("diffPath", new Class<?>[] { }));
		assertNotNull(route);
		
		view = route.execute(emptyMap);
		assertEquals(view.getResult(), "diffpath");
	}
	
	@Test
	public void testExecuteNonEmptyParamMapMethod() throws Exception {
		route = new Action("nonEmptyParam", object, TestController.class.getDeclaredMethod("nonEmptyParam", String.class));
		assertNotNull(route);
		
		nonEmptyMap.put("param", new String[] {"Smith"});
		view = route.execute(nonEmptyMap);
		assertEquals(view.getResult(), "nonEmptyParam");
	}

}
