package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import exceptions.RouteNotFoundException;
import router.Action;
import router.Router;

public class RouterTest {
	private static Router router;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		router = new Router("test.controllers");
	}

	@Test(expected = RouteNotFoundException.class)
	public void testGetEmptyRoute() throws RouteNotFoundException {
		router.getRoute("");
	}
	
	@Test(expected = RouteNotFoundException.class)
	public void testGetNonExistentRoute() throws RouteNotFoundException {
		router.getRoute("GET/non/existent");
	}
	
	@Test
	public void testGetRouteNamedAfterMethod() throws RouteNotFoundException {
		assertNotNull(router.getRoute("GET/test/index"));
	}

	@Test
	public void testGetAnnotationRenamedRoute() throws RouteNotFoundException {
		assertNotNull(router.getRoute("GET/test/different/path"));
	}
	
	@Test
	public void testGetRouteWithParamMap() throws RouteNotFoundException {
		assertNotNull(router.getRoute("GET/test/non/empty/:param/map"));
	}
	
	@Test
	public void testGetRouteParamMap() throws RouteNotFoundException {
		assertNotNull(router);
	}
	
	@Test
	public void testPostRoute() throws RouteNotFoundException {
		assertNotNull(router.getRoute("POST/test/postIt"));
	}
	
	@Test
	public void testPostRouteRenamed() throws RouteNotFoundException {
		assertNotNull(router.getRoute("POST/test/to/post"));
	}
	
	@Test
	public void testPostRouteRenamedWithParameter() throws RouteNotFoundException {
		assertNotNull(router.getRoute("POST/test/post/:myname"));
	}
	
	@Test
	public void testGetMethodMissingParams() throws RouteNotFoundException {
		assertNotNull(router.getRoute("GET/test/missingMethodParams"));
	}
	
	@Test
	public void testDefaultNamedController() throws RouteNotFoundException {
		assertNotNull(router.getRoute("GET/TestController2/index2"));
	}
	
	@Test
	public void testDefaultNamedControllerWithRenamedMethod() throws RouteNotFoundException {
		assertNotNull(router.getRoute("GET/TestController2/test2ControllerMethod"));
	}
	@Test
	public void testCall() {
//		fail("Not yet implemented");
	}

}
