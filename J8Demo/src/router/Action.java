package router;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import views.View;

public class Action {

	private String path;
	private Method method;
	private Object object;
	private Parameter[] parameters;
	private Binder binder;
	private Map<String, String[]> paramMap = new HashMap<String, String[]>();
	
	public Action(String path, Object object, Method method) {
		this.path = path;
		this.object = object;
		this.method = method;
		this.parameters = method.getParameters();
		
		this.binder = new Binder(this);
		
		for (Parameter param: parameters) {
			paramMap.put(param.getName(), new String[] {});
		}
	}
	
	public String getPath() {
		return path;
	}
	
	public Map<String, String[]> getParamMap() {
		return paramMap;
	}
	
	public Map<String, String[]> bind(HttpServletRequest request) {
		return binder.bind(request);
	}
	
	public View execute(Map<String, String[]> argumentsMap) {
		String[] beforeBind = null;
		Object[] afterBind = new String[argumentsMap.size()];
		
		int i = 0;
		// Temporary kluge - remove parameters from array.
		for (Parameter parameter : parameters) {
			beforeBind = argumentsMap.get(parameter.getName());
			if (beforeBind.length == 1) {
				afterBind[i] = beforeBind[0];
				i++;
			}
		}
		
		View view = null;
		try {
			view = (View)method.invoke(object, afterBind);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return view;
		
	}
}
