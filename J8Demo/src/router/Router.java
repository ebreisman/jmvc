package router;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.reflections.Reflections;

import views.View;
import annotations.Controller;
import annotations.DELETE;
import annotations.GET;
import annotations.POST;
import annotations.PUT;
import exceptions.RouteNotFoundException;

public class Router {

	private Map<String, Action> routes = new HashMap<String, Action>();
	
	/**
	 * Initialize the router with the regular expressions used to lookup a given action, and create those
	 * actions within the router.
	 * 
	 * @param controllerPackage the java package that contains the applications controllers.
	 */
	public Router(String controllerPackage) {
		Reflections reflect = new Reflections(controllerPackage);
		Set <Class<?>> controllers = reflect.getTypesAnnotatedWith(annotations.Controller.class);
		
		for (Class<?> controller : controllers) {
			if (controller.getAnnotation(Controller.class) != null) {
				Method[] methods = controller.getDeclaredMethods();
				
				// Create only one object for each controller.
				Constructor<?> ctor = null;
				try {
					ctor = controller.getConstructor(new Class<?>[] { });
				} catch (NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
				
				Object object = null;
				try {
					object = ctor.newInstance(new Object[] { });
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
				
				for (Method method : methods) {
					if (method.getAnnotation(GET.class) != null
						|| method.getAnnotation(POST.class) != null
						|| method.getAnnotation(PUT.class) != null
						|| method.getAnnotation(DELETE.class) != null) 
					{
						String path = getPath(controller, method);
						String regexPath = regexifyPath(path);
						Action route = new Action(path, object, method);
						routes.put(regexPath, route);
					}
				}
			}
		}
	}
	
	/**
	 * Determine regex path given controller name and annotations and method name and annotations.
	 * 
	 * @param controller the class object representing a given controller
	 * @param method the method object representing a particular action with the controller
	 * @return the path associated with the given controller/method combination.
	 */
	private String getPath(Class<?> controller, Method method) {
		String controllerName = null;
		if (controller.getAnnotation(Controller.class).value().isEmpty()) {
			controllerName = controller.getSimpleName();
		} else {
			controllerName = controller.getAnnotation(Controller.class).value();
		}
		
		String verb = null;
		String methodName = null;
		if (method.getAnnotation(GET.class) != null) {
			verb = "GET";
			if (method.getAnnotation(GET.class).value().isEmpty()) {
				methodName = method.getName();
			} else {
				methodName = method.getAnnotation(GET.class).value();
			}
		} else if (method.getAnnotation(POST.class) != null) {
			verb = "POST";
			if (method.getAnnotation(POST.class).value().isEmpty()) {
				methodName = method.getName();
			} else {
				methodName = method.getAnnotation(POST.class).value();
			}
		} else if (method.getAnnotation(PUT.class) != null) {
			verb = "PUT";
			if (method.getAnnotation(PUT.class).value().isEmpty()) {
				methodName = method.getName();
			} else {
				methodName = method.getAnnotation(PUT.class).value();
			}
		} else if (method.getAnnotation(DELETE.class) != null) {
			verb = "DELETE";
			if (method.getAnnotation(DELETE.class).value().isEmpty()) {
				methodName = method.getName();
			} else {
				methodName = method.getAnnotation(DELETE.class).value();
			}
		}
		
		return verb.toString() + "/" + controllerName + "/" + methodName;
	}
	
	/**
	 * Given a path expression, create it's regex so that it can be looked up in the router map.
	 * 
	 * @param path the expression used to map a path and the path's variables
	 * @return the regex that matches a given path's expression
	 */
	private String regexifyPath(String path) {
		String regexPath = null;
		
		String starPattern = Pattern.quote("*");
		String[] starSplit = path.split(starPattern);
		if (starSplit.length == 1) {
			regexPath = path.replaceAll("(:[a-zA-Z_$]+[a-zA-Z0-9]*)", ".+");
		} else if (starSplit.length == 2) {
			regexPath = path.replaceAll(starPattern + "([a-zA-Z_$]+[a-zA-Z0-9]*)", ".+");
		}

		return regexPath;
	}
	
	/**
	 * Find the action associated with a give reques path
	 * 
	 * @param path the request path
	 * @return the action associated with the requested path
	 * @throws RouteNotFoundException
	 */
	public Action getRoute(String path) throws RouteNotFoundException {
		Action route = routes.get(regexifyPath(path));
		if (route != null) {
			return route;
		} else {
			throw new RouteNotFoundException(path + " not found.");
		}
	}
	
	/**
	 * Execute a given route's controller.method() by pattern matching the regex route path to the route object.
	 * 
	 * @param request the request object
	 * @return the View object from that request
	 * @throws Exception
	 */
	public View call(HttpServletRequest request) throws Exception {
		View view = null;
		
		// Add any url query parameters to the map.
		for (Map.Entry<String, Action> route : routes.entrySet()) {
			if (Pattern.matches(route.getKey(), (request.getMethod() + request.getPathInfo()))) {
				Action action = route.getValue();
				Map<String, String[]> map = action.bind(request);
				try {					
					view = action.execute(map);
					break;
				} catch (NullPointerException e) {
					throw new RouteNotFoundException("No such route.");
				}
			}
		}
		
		return view;
	}
	
	/**
	 * Provide a list of path regex => path expressions contained in the router.
	 * 
	 * @return regex => path
	 */
	public List<String> getRoutes() {
		List<String> results = new ArrayList<String>();
		for (Map.Entry<String, Action> entry : routes.entrySet()) {
			results.add(entry.getKey() + " => " + entry.getValue().getPath());
		}
		return results;
	}
	
}
