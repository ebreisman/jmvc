package router;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class Binder {
	private Action action;
	
	public Binder(Action action) {
		this.action = action;
	}
	
	/**
	 * Bind the request variables to the action's/method's parameters.
	 * 
	 * @param request the http request
	 * @return a map of the action's/method's parameters to the request variables.
	 * @throws IOException 
	 */
	public Map<String, String[]> bind(HttpServletRequest request) {
		Map<String, String[]> paramMap = action.getParamMap();
		Map<String, String[]> pathMap = new HashMap<String, String[]>();
				
		String starPattern = Pattern.quote("*");
		String[] starSplit = action.getPath().split(starPattern);
		
		// If there is no '*' in the path, then split using the '/' delimiter and assign the variable names
		// and values using the ':' delimiter, else bind everything after the '*' to the only method parameter.
		if (starSplit.length == 1) {
			String[] pathVariableNames = action.getPath().split("/");
			String[] pathVariableValues = request.getPathInfo().split("/");
			
			for (int i = 0; i < pathVariableNames.length; i++) {
				if (pathVariableNames[i].matches(":[a-zA-Z_$]+[a-zA-Z_$0-9]*")) {
					pathMap.put(pathVariableNames[i].replace(":", ""), new String[] {pathVariableValues[i]});
				}
			}
		} else if (starSplit.length == 2) {
			String[] variableName = action.getPath().split(starPattern);
			String[] variableValue = (request.getMethod() + request.getPathInfo()).split(variableName[0]);
			
			pathMap.put(variableName[1], new String[] {variableValue[1]});
		}

		Map<String, String[]> queryMap = request.getParameterMap();
		
		Map<String, String[]> map = new HashMap<String, String[]>();
		map.putAll(paramMap);
		map.putAll(pathMap);
		map.putAll(queryMap);
		
		return map;
	}
}
