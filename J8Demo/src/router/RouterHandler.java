package router;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import views.View;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;

public class RouterHandler extends AbstractHandler {

	private Router router;
	
	public RouterHandler(Router router) {
		this.router = router;
	}
	
	@Override
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException 
	{
		View view = null;
		try {
			view = router.call(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.setContentType("text/html;charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);

		baseRequest.setHandled(true);
		
		Handlebars hb = new Handlebars();
		Template template = hb.compileInline("{{this}}!");
		response.getWriter().println(template.apply(view.getResult()));

	}

}
