package controllers;

import views.View;
import annotations.*;
import main.*;

@Controller("user")
public class UserController {

	@GET("nope")
	public View index() {
		return new View("Index action");
	}
	
	@GET("list/:name/:id")
	public View list(String name, String id) {
		return new View("List action");
	}
	
	@GET("move/*myname")
	public View putAction(String myname) {
		return new View(myname);
	}
	
	@GET("hello/:name/world")
	public View hello(String name) {
		return new View("Hello " + name + " World");
	}
	
	public View getAction(int id) {
		return new View("Id is " + id);
	}
}
